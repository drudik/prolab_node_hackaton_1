const fs = require('fs');
const axios = require('axios');

const intervalReqTime = (15 * 60) * 60// minutes in ml/sec

const saveData = (city, temp, date) => {
    const today = new Date();
    const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    
    let text = `Завтра, ${date}, для міста - ${city}, температурна позначка сягне відмиткі ${Math.round(temp)} за цельсіем.`;
    let reqTime = `Час запиту ${time}`;
    console.log(text, reqTime);
    fs.appendFile('message.txt', `${text} ${reqTime}\n`, (err) => {
        if (err) {
            throw err;
            return;
        } 
        console.log('файл записано');
    });
} 

const weatherInfo = async () => {
    const req = await axios.get('http://api.openweathermap.org/data/2.5/forecast?q=Kiev&cnt=7&lang=ua&units=metric&appid=ee106edfac919187044c7616c5cecc91');
    const city = req.data.city.name;
    const temp = req.data.list[6].main.temp;
    const date = req.data.list[6].dt_txt;
    saveData(city, temp, date)
}

weatherInfo();
const timer = setInterval(weatherInfo, 5000);

// 1  получить погоду с апи. на завтра, для своего города
// 2 сделать так, чтобы получало погоду каждые 15 минут
// 3 записывать погоду в какойто файл (так чтобы он дописывался, когда с апи запросили новую погоду. Должно быть дата + температура)


